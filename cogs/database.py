import discord
from discord.ext import commands
import pathlib

from cogs import messaging
from cogs.admin import author_is_plomdawg
from pony import orm
from datetime import date, timedelta


# Configure the pony database.
db = orm.Database()
db.bind(provider='sqlite', filename='database.sqlite', create_db=True)

class Guild(db.Entity):
    id = orm.PrimaryKey(str)
    prefix = orm.Required(str, default=';')
    volume = orm.Required(float, default=20.0)
    music_channel = orm.Optional(str)
    language = orm.Required(str, default='english')
    premium_until = orm.Required(date, default=date(2020, 4, 20))

    def __init__(self, **kwargs):
        # Call the usual init function.
        super().__init__(**kwargs)
        # Give each new guild 7 days of premium.
        self.premium_until = date.today() + timedelta(days=7)

    @property
    def premium(self):
        return (self.premium_until - date.today()).days >= 0

class User(db.Entity):
    id = orm.PrimaryKey(str)
    steam_id = orm.Optional(str, default='')

class Song(db.Entity):
    id = orm.PrimaryKey(str)
    title = orm.Required(str)
    duration = orm.Required(int, default=0)
    plays = orm.Required(int, default=0)
    query = orm.Optional(str)
    spotify_id = orm.Optional(str)
    thumbnail = orm.Optional(str)
    spotify_url = ""
    position = 0
    user = None

    @property
    def downloaded(self) -> bool:
        return self.path.is_file()

    @property
    def path(self) -> pathlib.Path:
        return pathlib.Path("songs") / f'{self.id}.mp3'

    @property
    def link(self, max_length=1000) -> str:
        # Remove brackets from song title.
        title = self.title.translate(str.maketrans(dict.fromkeys('[]()')))
        return f"[**{title}**]({self.youtube_url})"
        
    @property
    def youtube_url(self) -> str:
        return f"https://youtu.be/{self.id}"

class Database(commands.Cog):
    def __init__(self, bot):
        """
        Database Cog.
        """
        self.bot = bot
        self.db_session = orm.db_session
        db.generate_mapping(create_tables=True)

    def find(self, table, key, value, create_if_missing=False):
        """
        Find something in a table by key/value.
        Examples:
            find("Song", "title", "bad guy")
            find("Guild", "id", "1234567890")
        Returns None if not found.
        """
        if table == "Guild":
            db_generator = Guild
        elif table == "User":
            db_generator = User
        elif table == "Song":
            db_generator = Song
        else:
            raise(Exception(f"find() called with invalid table: {table}"))

        # Convert IDs from int to string.
        if key in ["id", "music_channel"]:
            value = str(value)
        
        # Query the database.
        with orm.db_session:
            result = orm.select(obj for obj in db_generator if getattr(obj, key) == value).first()

            # Create the missing result if desired.
            if result is None and create_if_missing is True:
                result = db_generator(**{key: value})

        return result

    def update(self, table, key, value, k, v, create_if_missing=False):
        """
        Updates a field in a table by key/value.
        Args:
            table: (str) The table to query. ["Guild", "User", "Song"]
            key: The search key to match on.
            value: The value of the search key.
            k: The key to update.
            v: The new value of k.
        Examples:
            update("Song", "title", "bad guy", "plays", 2)
            update("Guild", "id", "1234567890", "prefix", "!")
        """
        if table == "Guild":
            db_generator = Guild
        elif table == "User":
            db_generator = User
        elif table == "Song":
            db_generator = Song
        else:
            raise(Exception(f"update() called with invalid table: {table}"))

        # Convert IDs from int to string.
        if key in ["id", "music_channel"]:
            value = str(value)
        if k in ["id", "music_channel"]:
            v = str(v)

        # Query the database.
        with orm.db_session:
            result = orm.select(obj for obj in db_generator if getattr(obj, key) == value).first()

            # Create the missing result.
            if result is None:
                result = db_generator(**{key: value})

            # If the value is a function like (lambda count: count + 1)
            print(callable(v))
            if callable(v):
                # Apply the function to the old value.
                v = v(getattr(result, k))

            # Update the key.
            setattr(result, k, v)

        return result

    def save_song(self, id, title, duration, query, thumbnail, spotify_id="", plays=0):
        """ Saves a song to the database """
        with orm.db_session:
            song =  Song(
                id=str(id),
                title=str(title),
                duration=int(duration),
                plays=int(plays),
                query=str(query),
                spotify_id=str(spotify_id),
                thumbnail=str(thumbnail))
            return song

    async def load_song(self, song):
        """ Hackish way of storing spotify songs without looking them up on YouTube until needed """
        # This is a spotify song, load the youtube_id
        if type(song) is Song:
            return song
        user = song.user
        spotify_id = song.spotify_id
        spotify_url = song.spotify_url
        # Lookup the spotify ID in the database
        result = self.find("Song", "spotify_id", song.spotify_id)
        if result is not None:
            print(f"load_song() found cached spotify track: {result.title}")
            result.user = user
            return result
        
        # Not in database, find it on YouTube.
        song = await self.bot.get_cog('YouTube').query_to_song(song.query)
        
        # shitty error code handling
        if type(song) == int:
            return song
        
        # Save the spotify id to the database.
        song = self.bot.db.update("Song", "id", song.id, "spotify_id", spotify_id)
        song.user = user
        song.spotify_url = spotify_url
        return song

    @commands.command()
    async def prefix(self, ctx, *args):
        """ Set the prefix for a guild """
        # Help message (no args passed)
        if len(args) == 0:
            return await messaging.send(ctx, 'prefix usage', [ctx.prefix])

        # TODO: Validate prefix ?
        prefix = args[0]

        # Update the prefix setting in the database.
        self.update("Guild", "id", ctx.guild.id, "prefix", prefix)

        # Update cached prefixes
        self.bot.prefixes[ctx.guild.id] = prefix

        # Reply with status
        await messaging.send(ctx, "prefix set", [ctx.author.display_name, prefix])

    @commands.command()
    async def music(self, ctx, *args):
        """ Set the music channel for a guild """
    
        # Check database for current setting.
        guild = self.find("Guild", "id", ctx.guild.id, create_if_missing=True)

        # Music channel is either being set for the first time or being moved.
        if guild.music_channel == "" or guild.music_channel != str(ctx.channel.id):
            self.update("Guild", "id", ctx.guild.id, "music_channel", ctx.channel.id)
            await messaging.send(ctx, 'music channel set', [ctx.author.display_name, ctx.channel.name])
        
        # Command sent in music channel, disable forwarding.
        else:
            self.update("Guild", "id", ctx.guild.id, "music_channel", "")
            await messaging.send(ctx, 'music channel disabled', [ctx.author.display_name])

    @commands.command()
    async def language(self, ctx, *args):
        """ Sets the language for a guild """
        async with ctx.typing():
            if len(args) == 0:
                return await messaging.send(ctx, "language usage", [ctx.prefix])
            
            language = args[0].lower()

            if language not in [l.lower() for l in messaging.languages.keys()]:
                available_languages = '`' + '`,`'.join([l.title() for l in messaging.languages.keys()]) + '`'
                return await messaging.send(ctx, "language unknown", [language, available_languages])

            self.update("Guild", "id", ctx.guild.id, "language", language)

            return await messaging.send(ctx, "language set", [language.title()])

    @commands.command()
    @commands.check(author_is_plomdawg)
    async def premium(self, ctx, *args):
        """ Adds days of premium to a Guild. """
        usage = f'Usage: {ctx.prefix}premium [guild id] ["add" or "set"] [days]'
        if not ctx.guild:
            return await messaging.send(ctx, 'guild only')

        # No arguments, send usage.
        if len(args) == 0:
            return await ctx.send(usage)

        # First argument is guild ID. Make sure it is valid.
        guild_id = args[0]
        guild = self.bot.get_guild(int(guild_id))
        if not guild:
            return await ctx.send(f"Invalid guild ID: {guild_id}")

        # One argument, send guild premium status
        if len(args) == 1:
            _date = ctx.guild_conf.premium_until
            days_left = (_date - date.today()).days
            return await ctx.send(f'Premium for [**{guild.name}**] will expire on **{_date}** (**{days_left}** days left)')

        # Make sure 3 args are passed.
        if len(args) != 3:
            return await ctx.send(usage)
        (_, command, days) = args

        # Add to the premium days.
        if command == "add":
            # Guild already has premium, extend it.
            if ctx.guild_conf.premium:
                _date = ctx.guild_conf.premium_until + timedelta(int(days))
            # Guild doesn't have premium, start today.
            else:
                _date = date.today() + timedelta(int(days))
        # Set premium days left.
        elif command == "set":
            _date = date.today() + timedelta(int(days))
        # Invalid command.
        else:
            return await ctx.send(usage)

        # Update it in the database.
        self.update("Guild", "id", guild_id, "premium_until", _date)

        # Reply with the result.
        days_left = (_date - date.today()).days
        return await ctx.send(f'Premium for [**{guild.name}**] will expire on **{_date}** (**{days_left}** days left)')

    @commands.command()
    async def top(self, ctx, *args):
        """ Sends the top 10 most played songs"""
        async with ctx.typing():
            # Grab the top 10 songs from the database.
            num_songs = 10
            with orm.db_session:
                query = orm.select(song for song in Song).order_by(lambda song: song.plays * -1)
                songs = query[:num_songs]

            text = ""
            for song in songs:
                units = messaging.translate(ctx, "plays")
                title = messaging.format_title(song.title)[:50]
                text += f"[**{title}**]({song.youtube_url}) ({song.plays} {units})\n"
            title = messaging.translate(ctx, "top songs")
            return await self.bot.send_embed(ctx, title=title, text=text)

def setup(bot):
    print("Loading Database cog")
    bot.add_cog(Database(bot))
    print("Loaded Database cog")
