import asyncio
import os
import pathlib
import random
import time
import sys

import discord
import lyricsgenius
from discord.ext import commands

import keys
from cogs import messaging
from plombot import PlomContext


class SongQueue:
    def __init__(self, bot):
        self.bot = bot
        self.songs = []
        self.position = 0
        self.queue_message = None

    @property
    def next_song(self):
        if self.position < len(self.songs):
            return self.songs[self.position]
        return None

    def format_queue(self):
        """ Returns a song list with the current song highlighted """
        max_songs = 10
        song_list = ""

        # Find the start and end of what we should print
        queue_length = len(self.songs)

        # case 0: no songs in queue
        if len(self.songs) == 0:
            return "(empty)"

        # case 1: queue is shorter than max songs, print the whole thing
        if queue_length <= max_songs:
            start = 0
            end = queue_length

        # case 2: less than max_songs away from the current song - print old songs
        elif self.position-1+max_songs > queue_length:
            start = queue_length - max_songs
            if start < 0:
                start = 0
            end = queue_length

        # case 3: more than max_songs away from current song - print current and next few
        else:
            start = max(0, self.position-1)
            end = self.position + max_songs

        for i, song in enumerate(self.songs[start:end]):
            # Nicely format the duration
            duration = messaging.format_duration(song.duration)

            # Limit name length
            length = 41 - len(duration)

            # Add a triangle to the current song
            symbol = "⭄" if self.position == start + i else "--"

            # Remove brackets from song title and limit length
            title = song.title[:length].translate(str.maketrans(dict.fromkeys('[]()')))

            song_list += " {} {} [{}]({}) ({})\n".format(
                symbol, start+i, title, song.youtube_url, duration)

            if i == max_songs:
                break

        return song_list

    async def clear(self):
        self.position = 0
        self.songs = []
        await self.update_queue_message()

    async def queue(self, songs, user, insert=False):
        """ Adds songs to the queue, if insert is true they will play next """
        if insert:
            for i, song in enumerate(songs):
                song.user = user
                self.songs.insert(self.position + 1 + i, song)
        else:
            for song in songs:
                song.user = user
                self.songs.append(song)
        await self.update_queue_message()

    async def send_queue_message(self, channel):
        """ Deletes existing queue message and sends a new one.

        Args:
            channel: discord.TextChannel to send the message
        """

        # Delete old message
        await self.bot.delete_message(self.queue_message)
        self.queue_message = None

        # Send the new message
        embed = discord.Embed(color=0x22FF33, title="Song Queue ♫")
        embed.description = self.format_queue()
        self.queue_message = await channel.send(embed=embed)
            
        # Non-empty queue - add shuffle button
        if self.songs:
            await self.bot.add_reactions(self.queue_message, "🔀")

        return self.queue_message

    async def shuffle(self):
        """ Shuffles the songs beyond the current position """
        if len(self.songs) > self.position+1:
            temp = self.songs[self.position+1:]
            random.shuffle(temp)
            self.songs[self.position+1:] = temp
            
        await self.update_queue_message()

    async def update_queue_message(self):
        """ Updates Queue message if it exists. """
        if self.queue_message is not None and self.queue_message.embeds:
            embed = self.queue_message.embeds[0]
            embed.description = self.format_queue()
            try:
                await self.queue_message.edit(embed=embed)
            except discord.errors.NotFound:
                pass


class MusicPlayer:
    def __init__(self, bot, guild, volume=20):
        self.bot = bot
        self.guild = guild
        self.volume = 20
        self.queue = SongQueue(bot)
        self.np_message = None     # (discord.Message) last printed Now Playing message
        self.volume_message = None # (discord.Message) last printed volume
        self.play_lock = False
        self.youtube = bot.get_cog('YouTube')
        self.vc = None

        self.repeat = False
        self.repeat_one = False

    async def increment_position(self):
        """ Used by play when going to the next song. """
        # Do not increment position if repeat one is set
        if self.repeat_one:
            return

        self.queue.position += 1
        # Hit the end of the queue
        if len(self.queue.songs) <= self.queue.position:
            # Repeat
            if self.repeat:
                self.queue.position = 0

    async def connect(self, voice_channel):
        """ Connects to a voice channel. Returns the voice channel or None if error """
        # Find the voice client for this server
        if self.vc is None:
            self.vc = discord.utils.get(self.bot.voice_clients, guild=voice_channel.guild)

        if self.vc is None:
            #print("Connecting to voice channel:", voice_channel)
            try:
                self.vc = await voice_channel.connect()
                while not self.vc.is_connected():
                    await asyncio.sleep(1)
                #print("Successfully connected to:", self.vc.channel.name)
            except discord.errors.ClientException:
                pass
                #print("Already connected")
            except asyncio.TimeoutError:
                pass
                #print("Timed out!")

        # Move to the user if nobody is in the room with the bot
        if self.vc is not None and len(self.vc.channel.members) == 1:
            #print("Moving to", voice_channel)
            await self.vc.move_to(voice_channel)

        return self.vc

    async def play(self, voice_channel, text_channel, retry=True):
        """ Plays through the song queue
        :param channel: discord.TextChannel to send now playing message
        """
        # Allow one thread in here at a time
        if self.play_lock:
            return
        self.play_lock = True

        # Require voice client to exist
        if self.vc is None:
            print("Voice client does not exist, connecting")
            await self.connect(voice_channel)

        ## Require voice client to be connected
        if self.vc is None or not self.vc.is_connected(): 
            print("Voice client is not connected. Trying again.")
            await self.vc.disconnect(force=True)
            self.vc = None
            await self.connect(voice_channel)

        # Do nothing if already playing
        try:
            if self.vc.is_playing():
                print("Player already playing")
                self.play_lock = False
                return
        except AttributeError:
            print("Player is None")
            self.play_lock = False
            return

        # Change status text to "Now playing"
        if self.np_message is not None and self.np_message.embeds:
            try:
                embed = self.np_message.embeds[0]
                title = "Now Playing ♫"
                if not title == embed.title:
                    embed.title = title
                    await self.np_message.edit(embed=embed)
            except discord.errors.NotFound:
                pass

        # Player was previously paused
        if self.vc.is_paused():
            self.vc.resume()
            print("Played was paused, resuming.")
            self.play_lock = False
            return

        # Delete now playing message if no song in queue
        if self.queue.next_song is None:
            print(f"[{text_channel.guild.name}] Nothing left in queue")
            await self.queue.update_queue_message()
            await self.bot.delete_message(self.np_message)
            self.np_message = None
            self.play_lock = False
            return

        # Grab the next song and get it ready
        try:
            song = self.queue.next_song
        except IndexError:
            await text_channel.send(f"Something went wrong fetching song from queue (Error code: {len(self.queue.songs)} {self.queue.position})")
            self.play_lock = False
            return
        
        # Load all the fields (converts dicts to Song() objects)
        song = await self.bot.db.load_song(song)
        
        # todo: not this
        # handle error codes, poorly
        if type(song) == int:
            response = messaging.translate(ctx, "quota limit", song)
            embed = discord.Embed(description=response)
            return await text_channel.send(embed=embed)

        # Stick the loaded song back in the queue
        self.queue.songs[self.queue.position] = song

        # Get the mp3 ready
        try:
            await self.download_song(song)
        except Exception as error:
            # Something went wrong downloading.
            await text_channel.send(f"Failed to download `{song.title}`, skipping. Error:\n```{error}```")
            
            # Skip this song.
            await self.increment_position()
            self.play_lock = False
            return await self.play(voice_channel=voice_channel, text_channel=text_channel)

        # Log song info
        print(f"[{text_channel.guild.name}] ({song.user.display_name}) playing {song.title} ({song.plays} plays) from {song.path}")

        # Create the audio source. FFmpegPCMAudio reference:
        # https://discordpy.readthedocs.io/en/latest/api.html#discord.FFmpegPCMAudio
        options = f"-ss {song.position} -af loudnorm=I=-16.0:TP=-1.0"
        #try:
        audio_source_raw = discord.FFmpegPCMAudio(source=song.path, options=options)
        #except discord.errors.ClientException:
        #    audio_source_raw = discord.FFmpegPCMAudio(source=song.path, executable="C:/ffmpeg/bin/ffmpeg.exe", options=options)

        # Set the volume. PCMVolumeTransformer reference:
        # https://discordpy.readthedocs.io/en/latest/api.html#discord.PCMVolumeTransformer
        audio_source = discord.PCMVolumeTransformer(audio_source_raw, volume=self.volume / 100.0)

        # Begin playback or bust
        try:
            self.vc.play(audio_source)
        except:
            # Force disconnect
            await self.vc.disconnect(force=True)
            self.vc = None
            
            # Release the play lock
            self.play_lock = False

            # Reply with the error message
            error = sys.exc_info()[0]
            await text_channel.send(f"Failed to play `{song.title}`, skipping. Error:\n```{error}```")
            
            # Try again
            if retry is True:
                return await self.play(voice_channel=voice_channel, text_channel=text_channel, retry=False)
            else:
                raise(error)
        
        # Send now-playing message and update queue
        await self.send_now_playing(text_channel=text_channel)
        await self.queue.update_queue_message()

        # Wait for it to finish
        try:
            while self.vc and self.vc.is_playing():
                await asyncio.sleep(2)
        except AttributeError:
            pass

        # Player was stopped or paused
        if self.vc is None or self.vc.is_paused():
            self.play_lock = False
            return

        # Song finished playing - increment playcount in database
        self.bot.db.update("Song", "id", song.id, "plays", lambda count: count + 1)

        # Go on to the next song
        await self.increment_position()
        # Update queue message
        await self.queue.update_queue_message()
        self.play_lock = False
        await self.play(voice_channel=voice_channel, text_channel=text_channel)

    async def pause(self):
        """ Pauses voice client and updates the Now Playing message """
        if self.vc is not None and self.vc.is_playing():
            self.vc.pause()
        if self.np_message and self.np_message.embeds:
            try:
                embed = self.np_message.embeds[0]
                title = "Now Paused ♫"
                if not title == embed.title:
                    embed.title = title
                    await self.np_message.edit(embed=embed)
            except discord.errors.NotFound:
                pass

    async def set_volume(self, volume):
        """ Sets the player's volume in range [0,100] """
        self.volume = max(min(100, volume), 0)

        # Change current audio source volume
        if self.vc and self.vc.source:
            self.vc.source.volume = self.volume/100.0

        return self.volume

    async def skip(self, n=1):
        """ Skips n songs """
        print(f"skipping {n} songs")
        index = self.queue.position + n
        # Something is playing, it will increment pos by 1 when we stop the queue
        if self.vc and self.vc.is_playing():
            index = max(0, index - 1)
            self.queue.position = min(index, len(self.queue.songs))
            self.vc.stop()
        else:
            self.queue.position = min(index, len(self.queue.songs))
        print("skipped to:", self.queue.position)

    async def skipto(self, index):
        """ Skips to the given index """
        print("skipping to ", index, min(index, len(self.queue.songs)))
        # Something is playing, it will increment pos by 1 when we stop the queue
        if self.vc and self.vc.is_playing():
            index = max(0, index - 1)
            self.queue.position = min(index, len(self.queue.songs))
            self.vc.stop()
        else:
            self.queue.position = min(index, len(self.queue.songs))
        print("skipped to:", self.queue.position)

    async def start_over(self, n=1):
        """ Skips n songs """
        # Something is playing, start the song over.
        if self.vc and self.vc.is_playing():
            # End the song without incrementing the queue position.
            self.vc.stop()

    async def stop(self):
        """ Clears queue and disconnects, deleting all messages """
        await self.queue.clear()
        if self.vc is not None:
            await self.vc.disconnect(force=True)
            self.vc = None
        await self.bot.delete_message(self.np_message)
        await self.bot.delete_message(self.queue.queue_message)
        self.np_message = None
        self.queue_message = None

    async def download_song(self, song):
        """
        Downloads the .mp3 file from YouTube.
        Returns the file path for command chaining.
        """
        if song.downloaded:
            return song.path

        # Create the song directory.
        pathlib.Path("./songs").mkdir(parents=True, exist_ok=True)
    
        # Download using youtube-dl
        self.youtube.downloader.download([song.youtube_url])

        # Rename to song_id.mp3
        output = pathlib.Path(song.id) # output file is the video id
        if output.is_file:
            output.rename(song.path)

    async def send_now_playing(self, text_channel):
        """ Sends a Now Playing message, if possible also deletes the last one """
        # Delete the last message if it exists
        await self.bot.delete_message(self.np_message)
        self.np_message = None

        # Queue is empty, delete previous now_playing messages
        if self.queue.next_song is None:
            await text_channel.send("Nothing is playing.")
            return

        # Make sure the song is loaded before we print info.
        song = await self.bot.db.load_song(self.queue.next_song)
        
        # todo: not this
        # Handle error codes, poorly.
        if type(song) == int:
            return

        # Add user's name and song duration to footer.
        duration = messaging.format_duration(song.duration - song.position)
        footer = f"@{song.user.display_name} ({duration})"

        # Add "Up next" to footer if something is in the queue
        if len(self.queue.songs) > self.queue.position + 1:
            footer += " Up next: {}".format(self.queue.songs[self.queue.position+1].title)

        # Send a new message.
        self.np_message = await self.bot.send_embed(channel=text_channel,
                                                    color=0xFF69B4,
                                                    footer=footer,
                                                    footer_icon=song.user.avatar_url_as(size=64),
                                                    text=song.link,
                                                    thumbnail=song.thumbnail,
                                                    title="Now Playing ♫")

        # Add emoji controls.
        await self.bot.add_reactions(self.np_message, "⏸▶⏮️⏭⏹🇶🔊📝")
        return self.np_message

    async def send_volume(self, text_channel):
        """ Sends a volume message, if possible also deletes the last one """
        await self.bot.delete_message(self.volume_message)
        self.volume_message = None

        # Send the new message
        self.volume_message = await self.bot.send_embed(channel=text_channel,
                                                        color=0x22FF33,
                                                        title=f"Current volume : {int(self.volume)}%",
                                                        text=volume_bar(self.volume))
        # Add emoji controls
        await self.bot.add_reactions(self.volume_message, "⏬⬇⬆⏫✳")

    async def update_volume_message(self, user):
        """ Updates the last sent volume message """
        if self.volume_message is not None:
            embed = self.volume_message.embeds[0]
            embed.title = f"Current volume : {int(self.volume)}%"
            embed.description = volume_bar(self.volume)
            embed.set_footer(text=f"Changed by {user.display_name}", 
                             icon_url=user.avatar_url_as(size=64))
            try:
                await self.volume_message.edit(embed=embed)
            except discord.errors.NotFound:
                pass


class Music(commands.Cog):
    def __init__(self, bot):
        self.bot = bot
        self.music_players = {} # key = guild.id, value = music.MusicPlayer()
        self.bot.music_players = self.music_players
        self.genius = lyricsgenius.Genius(keys.genius_key)
        self.genius.verbose = False # Turn off status messages
        self.youtube = self.bot.get_cog('YouTube')
        self.spotify = self.bot.get_cog('Spotify')

    async def args_to_songs(self, args):
        """ Converts a list of arguments to a list of Songs """
        songs = []
        query = ""

        for arg in args:
            # Spotify songs/playlists/albums
            if 'spotify.com' in arg:
                spotify_songs = self.spotify.url_to_songs(arg)
                songs.extend(spotify_songs)
            # YouTube videos/playlists
            elif 'youtube.com' in arg or 'youtu.be' in arg:
                youtube_songs = await self.youtube.url_to_songs(arg)
                songs.extend(youtube_songs)
            # Gather all non-links into one query string
            else:
                query = query + arg + " "

        query = query.strip().lower()
                
        # Search query.
        if len(query) > 0:
            # Check the database for the query.
            song = self.bot.db.find("Song", "query", query)
            if song:
                songs.append(song)

            # Not found in the database.
            else:
                song = await self.youtube.query_to_song(query)
                print(f"song: {song}")
                # shitty error code handling
                if type(song) == int:
                    return song
                if song:
                    songs.append(song)

        # Remove any failed songs.
        return [song for song in songs if song is not None]

    async def get_player(self, ctx):
        """ Finds or creates a guild's music player. """
        try:
            player = self.music_players[ctx.guild.id]
        except KeyError:
            # Create new MusicPlayer
            mp = MusicPlayer(self.bot, ctx.guild, ctx.guild_conf.volume)
            self.music_players[ctx.guild.id] = mp
            player = self.music_players[ctx.guild.id]
        return player

    async def find_music_channel(self, ctx):
        """ Returns the guild's music channel if it exists """
        # Check the guild's config.
        if ctx.guild_conf.music_channel:
            music_channel = self.bot.get_channel(int(ctx.guild_conf.music_channel))

            # Delete the message and mention the music channel if it doesn't match
            if music_channel and music_channel.id != ctx.channel.id and ctx.message.author != self.bot.user:
                await self.bot.delete_message(ctx.message)
                await ctx.send(f'{ctx.author.mention} {music_channel.mention}')

            return music_channel

        # Return the current channel if we failed to find a music channel
        return ctx.channel

    @commands.command()
    async def clear(self, ctx):
        async with ctx.typing():
            if not author_voice_connected(ctx):
                response = f"{ctx.author.display_name}, you must be in a voice channel to clear the queue."
                await self.bot.send_embed(channel=ctx, text=response, thumbnail="http://i.imgur.com/go67eLE.gif")
                return

            player = await self.get_player(ctx)
            await player.queue.clear()
            return await messaging.send(ctx, 'cleared queue', [ctx.author.display_name])

    @commands.command()
    async def lyrics(self, ctx, *args):
        """ Gets lyrics from Genius """
        async with ctx.typing():
            # Premium command
            #if not ctx.guild_conf.premium:
            #    response = messaging.translate(ctx, "premium", ["lyrics"])
            #    return await self.bot.send_embed(ctx, text=response)

            start_time = time.perf_counter()
            # no args, lookup current song title
            if len(args) == 0:
                player = await self.get_player(ctx)
                song = player.queue.next_song
                if song is None:
                    return await messaging.send(ctx, "lyrics not playing", [ctx])
                query = song.title
            else:
                query = " ".join(args)
                
            # Remove "official video/audio" from the query
            query = messaging.format_title(query)

            song = self.genius.search_song(query)
            if song is None:
                return await messaging.send(ctx, "lyrics not found", [query])
            
            # Cut the lyrics off at 4 messages
            text = song.lyrics[:8000]
            text += "\n\n[" + messaging.translate(ctx, "lyrics full") + f"]({song.url})"
            elapsed = round(time.perf_counter() - start_time, 2)
            footer = messaging.translate(ctx, "lyrics found", [elapsed])
            if elapsed < 0.1:
                footer += " (cached!)"
            await self.bot.send_embed(channel=ctx,
                                      title=song.title,
                                      text=text,
                                      footer=footer,
                                      color=0xffff64,
                                      thumbnail=song._body.get("song_art_image_thumbnail_url"))

    @commands.command(aliases=["np"])
    async def nowplaying(self, ctx):
        player = await self.get_player(ctx)
        await player.send_now_playing(ctx)

    @commands.command()
    async def pause(self, ctx):
        if not author_voice_connected(ctx):
            response = f"{ctx.author.display_name}, you must be in a voice channel to pause the music."
            return await self.bot.send_embed(channel=ctx, text=response, thumbnail="http://i.imgur.com/go67eLE.gif")

        player = await self.get_player(ctx)
        await player.pause()
        if ctx.author.id is not self.bot.user.id:
            return await messaging.send(ctx, "paused", [ctx.author.display_name])

    @commands.command(aliases=["p"])
    async def play(self, ctx, *args):
        """ Queues music from a Spotify link, Youtube link, or search query """
        music_channel = await self.find_music_channel(ctx)

        # Ensure the user is connected to a voice channel
        if not author_voice_connected(ctx):
            response = f"{ctx.author.mention} you must be in a voice channel to play music."
            await self.bot.send_embed(channel=music_channel, text=response, thumbnail="http://i.imgur.com/go67eLE.gif")
            return

        # Get the music player for this guild
        player = await self.get_player(ctx)

        # Iterate through each argument and figure out what the user wants
        if len(args) > 0:
            async with music_channel.typing():
                songs = await self.args_to_songs(args)
                
                if type(songs) == int:
                    response = messaging.translate(ctx, "quota limit", songs)
                    embed = discord.Embed(description=response)
                    return await music_channel.send(embed=embed)

                # No results from search
                if len(songs) == 0:
                    response = messaging.translate(ctx, "play not found", ['+'.join(args), ' '.join(args)])
                    embed = discord.Embed(description=response)
                    return await music_channel.send(embed=embed)

                # Response based on number of songs queued
                if len(songs) == 1:
                    response = messaging.translate(ctx, "play one", [songs[0].link])
                else:
                    response = messaging.translate(ctx, "play many", [len(songs)])

                # Add the songs to the queue
                await player.queue.queue(songs, user=ctx.author)

                # List songs queued.
                title = messaging.translate(ctx, "queued")
                embed = discord.Embed(description=response, title=title)                
                await music_channel.send(embed=embed)

        # Connect to the voice channel and play.
        await player.play(voice_channel=ctx.author.voice.channel, text_channel=music_channel)

    @commands.command(aliases=["pnext", "upnext"])
    async def playnext(self, ctx, *args):
        """ Queues music from a Spotify link, Youtube link, or search query, inserting them into the queue"""
        music_channel = await self.find_music_channel(ctx)

        # Ensure the user is connected to a voice channel
        if not author_voice_connected(ctx):
            response = f"{ctx.author.mention} you must be in a voice channel to play music."
            await self.bot.send_embed(channel=music_channel, text=response, thumbnail="http://i.imgur.com/go67eLE.gif")
            return

        # Require arguments
        if len(args) == 0:
            await music_channel.send(f"Usage: {ctx.prefix}{ctx.command} [link or query]")
            return

        # Get the music player for this guild
        player = await self.get_player(ctx)

        # Iterate through each argument and figure out what the user wants
        async with music_channel.typing():
            songs = await self.args_to_songs(args)
            
            if type(songs) == int:
                response = messaging.translate(ctx, "quota limit", songs)
                embed = discord.Embed(description=response)
                return await music_channel.send(embed=embed)

            # No results from search
            if len(songs) == 0:
                await music_channel.send(f"Could not find song from query: '{' '.join(args)}'")
                return

            # Response based on number of songs queued
            if len(songs) == 1:
                response = f"Queued {songs[0].link} up next"
            else:
                response = f"Queued {len(songs)} songs up next"

            # Add the songs to the queue
            await player.queue.queue(songs, user=ctx.author, insert=True)
            embed = discord.Embed(description=response)
            await music_channel.send(embed=embed)

        # Connect to the voice channel and play.
        await player.play(voice_channel=ctx.author.voice.channel, text_channel=music_channel)

    @commands.command(aliases=["palbum"])
    async def playalbum(self, ctx, *args):
        """ Plays an album from a search query. """
        music_channel = await self.find_music_channel(ctx)
        if len(args) == 0:
            await music_channel.send("Usage: {}playalbum [album name]".format(ctx.prefix))
            return

        if not author_voice_connected(ctx):
            response = f"{ctx.author.display_name}, you must be in a " \
                        "voice channel to play music.".format(ctx)
            await self.bot.send_embed(channel=ctx, text=response, thumbnail="http://i.imgur.com/go67eLE.gif")
            return

        query = ' '.join(args)
        try:
            album = self.spotify.query_to_album(query)
        except IndexError:
            await music_channel.send("Failed to find album from query \"{}\".".format(query))
            return
        async with music_channel.typing():
            player = await self.get_player(ctx)
            songs = self.spotify.album_to_songs(album_id=album['id'])

            if len(songs) > 0:
                await player.queue.queue(songs, user=ctx.author)
                response = f"Queued {len(songs)} songs from [{album['name']}]" \
                        f"(https://open.spotify.com/album/{album['id']}) by " \
                        f"[{album['artists'][0]['name']}]" \
                        f"(https://open.spotify.com/artist/{album['artists'][0]['id']})"
                await self.bot.send_embed(channel=music_channel, text=response)
            else:
                await music_channel.send(f"Failed to find songs from album [{album['name']}]" \
                            f"(https://open.spotify.com/album/{album['id']})")
                return

        # Connect to the voice channel and play.
        await player.play(voice_channel=ctx.author.voice.channel, text_channel=music_channel)

    @commands.command(aliases=["partist"])
    async def playartist(self, ctx, *args):
        """ Plays the top 10 songs of an artist. """
        async with ctx.typing():
            
            music_channel = await self.find_music_channel(ctx)

            if len(args) == 0:
                await music_channel.send(f"Usage: {ctx.prefix}playartist [artist name]")
                return

            if not author_voice_connected(ctx):
                response = "{0.author.display_name}, you must be in a " \
                           "voice channel to play music.".format(ctx)
                await self.bot.send_embed(channel=music_channel, text=response, thumbnail="http://i.imgur.com/go67eLE.gif")
                return

            player = await self.get_player(ctx)
            query = ' '.join(args)
            try:
                artist = self.spotify.query_to_artist(query)
            except IndexError:
                await music_channel.send("Failed to find artist from query \"{}\".".format(query))
                return

            songs = self.spotify.artist_top_songs(artist_id=artist['id'])

            if len(songs) == 0:
                response = "Failed to find songs from artist "
                response += "[{}](https://open.spotify.com/artist/{}).".format(artist['name'], artist['id'])
                await music_channel.send(response)
                return

            await player.queue.queue(songs, user=ctx.author)
            response = f"Queued {len(songs)} of [{artist['name']}]" \
                        f"(https://open.spotify.com/artist/{artist['id']})'s top tracks"
            await self.bot.send_embed(channel=music_channel, text=response)

        # Connect to the voice channel and play.
        await player.play(voice_channel=ctx.author.voice.channel, text_channel=music_channel)

    @commands.command(aliases=["pspotify", "ps"])
    async def playspotify(self, ctx, *args):
        """ Plays the current track the user is listening to on Spotify """
        # Get the user's activity
        activity = ctx.author.activity
        
        if type(activity) is discord.activity.Activity:
            return await messaging.send(ctx, "spotify wrong activity", [ctx.author.mention, activity.name])

        # Make sure they're listening to Spotify
        if type(activity) is not discord.activity.Spotify:
            return await messaging.send(ctx, "spotify no activity", [ctx.author.mention])

        # Convert the track ID to a spotify link
        url = f"https://open.spotify.com/track/" + activity.track_id

        # Call play command with modified context
        await self.play.callback(self, ctx, url)

    @commands.command()
    async def players(self, ctx):
        playing = []
        paused = []
        stopped = []
        # Traverse all music players
        for player in self.music_players.values():
            # If the player exists, add to appropriate list
            if player.vc is None:
                stopped.append(player)
            else:
                if player.vc.is_playing():
                    playing.append(player)
                elif player.vc.is_paused():
                    paused.append(player)
                else:
                    stopped.append(player)

        title = "Music Player Stats"
        text = f"Playing: {len(playing)}\n"
        text += f"Paused: {len(paused)}\n"
        text += f"Stopped: {len(stopped)}\n"
        await self.bot.send_embed(channel=ctx.channel, text=text, title=title)

    @commands.command(aliases=["q"])
    async def queue(self, ctx):
        """ Displays the current song queue with emoji controls """
        music_channel = await self.find_music_channel(ctx)
        async with music_channel.typing():
            player = await self.get_player(ctx)
            await player.queue.send_queue_message(music_channel)

    @commands.command(aliases=["rm", "rem"])
    async def remove(self, ctx, *args):
        """ Removes a song from the queue. """
        async with ctx.typing():
            try:
                index = int(args[0])
            except (IndexError, ValueError):
                return await messaging.send(ctx, "remove usage", [ctx.prefix])

            if not author_voice_connected(ctx):
                response = "{0.author.display_name}, you must be in a " \
                            "voice channel to remove songs from the queue.".format(ctx)
                await self.bot.send_embed(channel=ctx, text=response, thumbnail="http://i.imgur.com/go67eLE.gif")
                return

            player = await self.get_player(ctx)
            try:
                song = player.queue.songs.pop(index)
                response = "Removed: **{}** {}".format(index, song.link)
                await self.bot.send_embed(channel=ctx, text=response)
                await player.queue.update_queue_message()
            except IndexError:
                return await messaging.send(ctx, "remove failed", [index])

    @commands.command(aliases=["loop"])
    async def repeat(self, ctx):
        """ Sets the player to repeat the entire queue. """
        async with ctx.typing():
            music_channel = await self.find_music_channel(ctx)

            if not author_voice_connected(ctx):
                response = f"{ctx.author.display_name}, you must be in a " \
                           "voice channel to loop the current song."
                await self.bot.send_embed(channel=music_channel, text=response, thumbnail="http://i.imgur.com/go67eLE.gif")
                return

            player = await self.get_player(ctx)
            player.repeat = not player.repeat
            status = "on" if player.repeat else "off"
            response = f"{ctx.author.display_name} turned **{status}** repeat for the whole queue."
            if player.repeat_one:
                player.repeat_one = False
                response += " (and turned off song looping)"
        await music_channel.send(response)
    
    @commands.command(aliases=["loopone"])
    async def repeatone(self, ctx):
        """ Sets the player to repeat the current song. """
        async with ctx.typing():
            music_channel = await self.find_music_channel(ctx)

            if not author_voice_connected(ctx):
                response = f"{ctx.author.display_name}, you must be in a " \
                           "voice channel to loop the queue."
                await self.bot.send_embed(channel=music_channel, text=response, thumbnail="http://i.imgur.com/go67eLE.gif")
                return

            player = await self.get_player(ctx)
            player.repeat_one = not player.repeat_one
            status = "on" if player.repeat_one else "off"
            response = f"{ctx.author.display_name} turned **{status}** repeat for this song."
            if player.repeat:
                player.repeat = False
                response += " (and turned off queue looping)"
        await music_channel.send(response)

    @commands.command()
    async def resume(self, ctx):
        """ Resumes playback """
        async with ctx.typing():
            if not author_voice_connected(ctx):
                response = "{0.author.display_name}, you must be in a " \
                           "voice channel to resume the music.""".format(ctx)
                await self.bot.send_embed(channel=ctx, text=response, thumbnail="http://i.imgur.com/go67eLE.gif")
                return

            music_channel = await self.find_music_channel(ctx)
            player = await self.get_player(ctx)

            if not player.queue.next_song:
                return await messaging.send(ctx, "queue empty")

            player = await self.get_player(ctx)

        # Connect to the voice channel and resume playback
        await player.play(voice_channel=ctx.author.voice.channel, text_channel=music_channel)

    @commands.command()
    async def shuffle(self, ctx):
        """ Shuffles the queue """
        if not author_voice_connected(ctx):
            response = "{0.author.display_name}, you must be in a " \
                        "voice channel to shuffle the queue.""".format(ctx)
            await self.bot.send_embed(channel=ctx, text=response, thumbnail="http://i.imgur.com/go67eLE.gif")
            return

        music_channel = await self.find_music_channel(ctx)

        player = await self.get_player(ctx)
        await player.queue.shuffle()
        if ctx.author.id is not self.bot.user.id:
            await music_channel.send(f"{ctx.author.mention} shuffled the queue.")

    @commands.command(aliases=["next"])
    async def skip(self, ctx, *args):
        """ Skip the current song """
        if not author_voice_connected(ctx):
            response = "{0.author.display_name}, you must be in a " \
                        "voice channel to skip the song.""".format(ctx)
            await self.bot.send_embed(channel=ctx, text=response, thumbnail="http://i.imgur.com/go67eLE.gif")
            return

        player = await self.get_player(ctx)

        # Argument can be the number of songs to skip
        n_songs = 1
        if len(args) > 0:
            try:
                n_songs = int(args[0])
            except ValueError:
                return await messaging.send(ctx, "skip usage", [ctx])

        await player.skip(n_songs)
        await player.queue.update_queue_message()

    @commands.command(aliases=["goto"])
    async def skipto(self, ctx, *args):
        """ Skip to the specified song """
        if not author_voice_connected(ctx):
            response = "{0.author.display_name}, you must be in a " \
                        "voice channel to skip multiple songs.""".format(ctx)
            await self.bot.send_embed(channel=ctx, text=response, thumbnail="http://i.imgur.com/go67eLE.gif")
            return
        
        if len(args) == 0:
            return await messaging.send(ctx, "skipto usage", [ctx])

        try:
            index = int(args[0])
        except ValueError:
            return

        player = await self.get_player(ctx)
        await player.skipto(index)    
        await player.queue.update_queue_message()

    @commands.command(aliases=["leave", "scram", "dc", "disconnect", "end"])
    async def stop(self, ctx):
        """ Stop the player and clear the queue. """
        if not author_voice_connected(ctx):
            response = "{0.author.display_name}, you must be in a " \
                        "voice channel to stop the music.".format(ctx)
            await self.bot.send_embed(channel=ctx, text=response,
                                       thumbnail="http://i.imgur.com/go67eLE.gif")
            return

        player = await self.get_player(ctx)
        await player.stop()

    @commands.command(aliases=["stream"])
    async def video(self, ctx):
        """ Reply with a link that users may use to begin screen sharing to a channel """
        if not author_voice_connected(ctx):
            response = "{0.author.display_name}, you must be in a " \
                        "voice channel to use video.""".format(ctx)
            await self.bot.send_embed(channel=ctx, text=response, thumbnail="http://i.imgur.com/go67eLE.gif")
            return

        url = f"http://www.discordapp.com/channels/{ctx.guild.id}/{ctx.author.voice.channel.id}"
        response = f"[Click to join the video call in {ctx.author.voice.channel.name}]({url})"
        await self.bot.send_embed(channel=ctx, text=response)

    @commands.command(aliases=["v", "vol"])
    async def volume(self, ctx, *args):
        """ Sends an interactive volume message """
        if not author_voice_connected(ctx):
            response = "{0.author.display_name}, you must be in a " \
                        "voice channel to change the volume.".format(ctx)
            await self.bot.send_embed(channel=ctx, text=response, thumbnail="http://i.imgur.com/go67eLE.gif")
            return

        music_channel = await self.find_music_channel(ctx)

        player = await self.get_player(ctx)
        if len(args) > 0:
            try:
                await player.set_volume(float(args[0]))
            except:
                pass

        await player.send_volume(music_channel)

    @commands.Cog.listener()
    async def on_reaction_add(self, reaction, user):
        # Ignore own reactions
        if user == self.bot.user:
            return

        # Ignore messages not sent by the bot
        if reaction.message.author != self.bot.user:
            return

        # Create a discord context to pass onto commands
        ctx = PlomContext(
            message = reaction.message,
            bot = self.bot,
            prefix = ";",
            guild = user.guild,
            author = user
        )

        # Add guild config if this is a guild.
        if reaction.message.guild:
            ctx.guild_conf = self.bot.db.find("Guild", "id", user.guild.id, create_if_missing=True)

        # Get the music player
        player = await self.get_player(ctx)

        # -- NOW PLAYING CONTROLS --

        # QUEUE
        if reaction.emoji in '🇶':
            await self.queue.callback(self, ctx) # pylint: disable=no-member
        # PLAY (RESUME)
        elif reaction.emoji in '▶':
            await self.resume.callback(self, ctx) # pylint: disable=no-member
        # PAUSE
        elif reaction.emoji in '⏸':
            await self.pause.callback(self, ctx) # pylint: disable=no-member
        # LYRICS
        elif reaction.emoji in '📝':
            await self.lyrics.callback(self, ctx) # pylint: disable=no-member
        # SHUFFLE
        elif reaction.emoji in '🔀':
            await self.shuffle.callback(self, ctx) # pylint: disable=no-member
        # SKIP
        elif reaction.emoji in '⏭':
            await self.skip.callback(self, ctx) # pylint: disable=no-member
            await reaction.message.channel.send("{} didn't like that song.".format(user.display_name))
        # START OVER
        elif reaction.emoji in '⏮️':
            await self.start_over.callback(self, ctx) # pylint: disable=no-member
            await reaction.message.channel.send("{} started the song over.".format(user.display_name))
        # STOP
        elif reaction.emoji in '⏹':
            await self.stop.callback(self, ctx) # pylint: disable=no-member
            await reaction.message.channel.send("{} stopped the party.".format(user.display_name))
        # VOLUME
        elif reaction.emoji in '🔊':
            await self.volume.callback(self, ctx) # pylint: disable=no-member

        # -- VOLUME CONTROLS --

        elif reaction.emoji in '⬇️': # volume down
            await player.set_volume(volume=player.volume - 1.75)
            await player.update_volume_message(user=user)
        elif reaction.emoji in '⬆️': # volume up
            await player.set_volume(volume=player.volume + 1.75)
            await player.update_volume_message(user=user)
        elif reaction.emoji in '⏬': # volume down+
            await player.set_volume(volume=player.volume - 8)
            await player.update_volume_message(user=user)
        elif reaction.emoji in '⏫': # volume up+
            await player.set_volume(volume=player.volume + 8)
            await player.update_volume_message(user=user)
        elif reaction.emoji in '✳': # volume reset
            await player.set_volume(volume=20)
            await player.update_volume_message(user=user)
        else:
            # Unknown emoji, do nothing
            return

        # Remove the reaction once the job is done
        try:
            await reaction.remove(user)
        except discord.errors.NotFound:
            pass

    @commands.Cog.listener()
    async def on_message(self, message):
        if message.content == "@vc" and author_voice_connected(message):
            if len(message.author.voice.channel.members) == 1:
                response = "you are alone my dude :("
            elif len(message.author.voice.channel.members) == 2 and self.bot.user in message.author.voice.channel.members:
                response = "it's just us in here ;^)"
            else:
                response = ""
                for user in message.author.voice.channel.members:
                    if user != message.author:
                        response += user.mention + " "
            return await message.channel.send(response)

def author_voice_connected(ctx):
    """ Returns True if the author is connected to a voice channel. """
    try:
        return ctx.author is not None and ctx.author.voice is not None and ctx.author.voice.channel is not None
    except AttributeError:
        return False

def volume_bar(volume):
    """ Returns an ASCII volume bar  """
    text = ""
    vol = int(volume/4)
    text += "█" * vol
    text += "░" * (25-vol)
    return text

def setup(bot):
    cog = Music(bot)
    bot.add_cog(cog)
    print("Loaded Music cog")
