""" Youtube cog """
import html
import urllib

import isodate
import aiohttp
import youtube_dl
from discord.ext import commands
from cogs.database import Song

import keys

OPTIONS = {'format': 'bestaudio/best',
           'extractaudio' : True,
           'audioformat' : "mp3",
           'outtmpl': '%(id)s',
           'noplaylist' : True,
           'nocheckcertificate' : True,
           'no_warnings' : True,
          }

class YouTube(commands.Cog):
    def __init__(self, bot):
        self.bot = bot
        self.downloader = youtube_dl.YoutubeDL(OPTIONS)

    async def _get(self, endpoint, params=None):
        """ Makes an authorized request to the desired endpoint.

        Returns:
            JSONified response or None if it fails.
        """
        url = f"https://www.googleapis.com/youtube/v3/{endpoint}?key={keys.youtube_key}&"
        if params:
            url += urllib.parse.urlencode(params)
        js = None
        async with aiohttp.ClientSession() as session:
            async with session.get(url) as r:
                if r.status != 200:
                    return r.status
                js = await r.json()
                #print(js)
        return js

    async def url_to_songs(self, url):
        """ Returns a list of Song()s from given YouTube URL """
        if 'playlist' in url:
            songs = await self.playlist_to_songs(url)
        else:
            songs = await self.video_to_songs(url)
        return songs

    async def playlist_to_songs(self, url):
        """ Returns a list of Song() objects from a given youtube playlist """
        songs = []

        playlist_id = url.split('list=', 1)[1].split('&', 1)[0]

        # Get the list of videos in the playlist
        params = {
            "part": "contentDetails,snippet",
            "maxResults": 50,
            "playlistId": playlist_id,
        }
        response = await self._get("playlistItems", params)
        if response is not None:
            # Go through each page of results
            index = 0
            while index < response.get("pageInfo", {}).get("totalResults", 0):
                for item in response.get("items", []):
                    song = await self.video_item_to_song(item=item, query="")
                    songs.append(song)
                    index += 1
                try:
                    # Get next page using the token
                    params["pageToken"] = response["nextPageToken"]
                    response = await self._get("playlistItems", params)
                except KeyError:
                    # Last page
                    pass

        return songs

    async def id_to_song(self, id, query):
        # Search the database for the video ID.
        song = self.bot.db.find("Song", "id", id)
        if song:
            print(f"Found song in cache: {song}")
            return song
            
        # Search for the video on YouTube.
        search_parameters = {
            "part": "contentDetails,snippet",
            "id": id,
            }
        results = await self._get("videos", params=search_parameters)
        try:
            item = results.get("items", [{}])[0]
        except IndexError:
            return None
        snippet = item.get("snippet", {})
        title = html.unescape(snippet.get("title", "?"))
        title = title.translate(str.maketrans(dict.fromkeys('[]()')))
        thumbnails = snippet.get("thumbnails", {}).get("high", {})
        thumbnail = thumbnails.get("url", "https://i.imgur.com/MSg2a9d.png") # question-mark.png
        duration = item.get("contentDetails", {}).get("duration", 0)
        if duration != 0:
            duration = int(isodate.parse_duration(duration).total_seconds())
            print(f"duration: {duration}")

        # Save the song to the database and return the result.
        return self.bot.db.save_song(
            id=id,
            title=title,
            duration=duration,
            query=query,
            thumbnail=thumbnail
        )

    async def query_to_song(self, query):
        # Search the database for the query.
        song = self.bot.db.find("Song", "query", query)
        if song:
            return song

        # Search for the video on YouTube.
        search_parameters = {
                "part": "snippet",
                "type": "video",
                "maxResults": 1,
                "q": query,
                }
        results = await self._get("search", params=search_parameters)
        if results is not None:
            # shitty 403 quota limit handling
            if type(results) == int:
                return results
            try:
                youtube_id = results.get("items")[0].get("id", {}).get("videoId")
            except:
                return None

        # Failed to find a result.
        if youtube_id is None:
            return None

        # Get the rest of the video details using the video ID.
        return await self.id_to_song(id=youtube_id, query=query)

    async def video_to_songs(self, url):
        """ Converts a video URL to a list containing one Song """
        parsed = urllib.parse.urlparse(url)

        # "https://youtube.com/?v=videoID"
        try:
            youtube_id = urllib.parse.parse_qs(parsed.query).get('v')[0]
        except:
            youtube_id = None

        # "https://youtu.be/videoID"
        if youtube_id is None:
            youtube_id = parsed.path[1:]

        song = await self.id_to_song(id=youtube_id, query=url)

        if song is None:
            return []

        # Set song position to time offset
        try:
            song.position = int(urllib.parse.parse_qs(parsed.query).get('t')[0])
        except:
            song.position = 0

        return [song]

    async def video_item_to_song(self, item, query):
        """ Converts a YouTube response from the videos or playlistItems endpoint to a Song() """
        id = item.get("snippet", {}).get("resourceId", {}).get("videoId")
        if id is None:
            return None
        return await self.id_to_song(id, query)


def setup(bot):
    bot.add_cog(YouTube(bot))
    print("Loaded YouTube cog")
