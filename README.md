# plombot

A multi-language Discord music bot written in Python 3.8.

![](https://i.imgur.com/Vc14MOX.png)

Click [here](https://discordapp.com/oauth2/authorize?client_id=412809807842639883&scope=bot&permissions=1110453312) to invite plombot your server!
