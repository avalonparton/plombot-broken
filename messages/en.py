language = "english"

translations = {
    "cleared queue": "{} cleared the queue",

    "commands": "Commands",

    "guild only": "This command can only be used in servers.",

    "language set": "Set language to **{}**",
    "language usage": "Usage: {}language [new language]",
    "language unknown": [
        "Unknown language: **{}**.\n",
        "Choose one of the following: {}",
    ],

    "lyrics not found": "Failed to find lyrics for '{}'",
    "lyrics not playing": [
        "Nothing is playing right now.",
        "Try: `{0.prefix}{0.invoked_with} [song title]`",
    ],
    "lyrics full": "Full lyrics on Genius",
    "lyrics found": "Lyrics found in {} seconds.",

    "music channel disabled": "{} disabled music forwarding. (Send again to enable)",
    "music channel set": "{} enabled music forwarding to **{}**. (Send again to disable)",

    "paused": "{} paused the music",

    "prefix set": "{} set the prefix to `{}`",
    "prefix usage": "Usage: {}prefix [new prefix]",

    "premium": "This command ({}) requires [**premium**](https://discord.gg/Czj2g9c)!",

    "play not found": "Failed to find song on [YouTube](https://www.youtube.com/results?q={0}&sp=QgIIAQ%253D%253D): `{1}`",
    "play one": "Queued {}",
    "play many": "Queued **{}** songs",

    "plays": "plays",

    "queue empty": "Nothing is left in the queue.",

    "quota limit": "Error {}: YouTube Quota limit reached (20k requests per day). plomdawg is working on it.",

    "remove usage": "Usage: {}remove [song index]",
    "remove failed": "Failed to remove song at index {}",

    "skip usage": "Usage: {0.prefix}{0.invoked_with} (number of songs)",
    "skipto usage": "Usage: {0.prefix}{0.invoked_with} [song index]",

    "spotify no activity": [
        "{} Failed to detect Spotify activity through Discord. ",
        "Link your account in **User Settings** > **Connections** > **Spotify**, then play something!"
    ],
    "spotify wrong activity": [
        "{} Could not detect Spotify track, you may have to disable",
        " the integration `{}` in **User Settings** > **Game Activity**"
    ],

    "top songs": "Most played songs",

    "vote": "Vote for plombot on [Discord Bot List]({})",
}
